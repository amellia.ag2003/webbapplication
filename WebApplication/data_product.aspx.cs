﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class data_product : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            Product product = db.Products.FirstOrDefault(item => item.IDProduct == int.Parse(Request.QueryString["id"]));

            if (product != null)
            {
                IDProduct.Text = product.IDProduct.ToString();
                SKU.Text = product.SKU;
                Name.Text = product.Name;


                Submit.Text = "Ubah";
            }
            else
            {

            }
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            if (IDProduct.Text == string.Empty)
            {
                Product product = new Product
                {
                    SKU = SKU.Text,
                    Name = Name.Text,
                    Flag = true,
                    CreatedBy = "1",
                    CreatedAt = DateTime.Now
                };

                db.Products.InsertOnSubmit(product);
            }
            else
            {
                Product product = db.Products.FirstOrDefault(item => item.IDProduct == int.Parse(IDProduct.Text));

                product.SKU = SKU.Text;
                product.Name = Name.Text;

            }
            db.SubmitChanges();

            Response.Redirect("product.aspx");
        }
    }
}
