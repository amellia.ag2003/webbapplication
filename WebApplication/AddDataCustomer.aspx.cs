﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


    public partial class AddDataCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                Customer customer = new Customer
                {
                    Name = Name.Text,
                    Flag = true,
                    CreatedBy = "1",
                    CreatedAt = DateTime.Now
                };

                db.Customers.InsertOnSubmit(customer);
                db.SubmitChanges();

                Response.Redirect("Customers.aspx");
            }
        }
    }
