﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PengaturanProductDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using(DatabaseDataContext db = new DatabaseDataContext())
        {
            ProductDetail productDetail = db.ProductDetails.FirstOrDefault(item => item.IDProductDetails == int.Parse(Request.QueryString["id"]));

            if (productDetail != null)
            {
                IDProductD.Text = productDetail.IDProductDetails.ToString();
                Product.Text = productDetail.Product.ToString();
                Variant.Text = productDetail.Variant;
                Qty.Text = productDetail.Qty.ToString();
                Price.Text = productDetail.Price.ToString();

                //Product.DataSource = db.Products.ToArray();
                //Product.DataTextField = "Name";
                //Product.DataValueField = "IDProduct";
                //Product.DataBind();
                Submit.Text = "Ubah";
            }
            else
            {

            }
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            if (IDProductD.Text == string.Empty)
            {
                ProductDetail productDetail = new ProductDetail
                {
                    IDProduct = int.Parse(Product.SelectedValue),
                    Variant = Variant.Text,
                    Qty = int.Parse(Qty.Text),
                    Price = int.Parse(Price.Text),
                    Flag = true,
                    CreatedBy = "1",
                    CreatedAt = DateTime.Now
                };

                db.ProductDetails.InsertOnSubmit(productDetail);
            }
            else
            {
                ProductDetail productDetail = db.ProductDetails.FirstOrDefault(item => item.IDProduct == int.Parse(IDProductD.Text));

                productDetail.IDProduct = int.Parse(Product.SelectedValue);
                productDetail.Variant = Variant.Text;
                productDetail.Qty = int.Parse(Qty.Text);
                productDetail.Price = int.Parse(Price.Text);
            }
            db.SubmitChanges();

            Response.Redirect("ProductD.aspx");
        }
    }
}