﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



public partial class ProductD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loadData();
        }
    }
    private void loadData()
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            var data = db.ProductDetails.Select(item => new
            {
                Product = item.Product.Name,
                item.Variant,
                item.Qty,
                item.Price

            })
                    .ToArray();

            DataProductD.DataSource = data;
            DataProductD.DataBind();
        }
    }
    

    protected void DataProductD_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {
        
        {
            if (e.CommandName == "Ubah")
            {

            }
            else if (e.CommandName == "Delete")
            {
                using (DatabaseDataContext db = new DatabaseDataContext())
                { 
                    ProductDetail productdetail = db.ProductDetails.FirstOrDefault(item => item.IDProductDetails.ToString() == e.CommandArgument.ToString());
                    if(productdetail != null)
                    {
                        var data = db.TransactionDetails.Where(x => x.IDProductDetails == productdetail.IDProductDetails).ToArray();

                        db.ProductDetails.DeleteOnSubmit(productdetail);
                        db.TransactionDetails.DeleteAllOnSubmit(data);
                    }

                
                db.SubmitChanges();
                    
                loadData();

            }
        }
    }
}
}
