﻿using System;
using System.Linq;

namespace WebApplication
{
    public partial class data_transaksi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    IDCustomer.DataSource = db.Customers.ToArray();
                    IDCustomer.DataTextField = "Name";
                    IDCustomer.DataValueField = "IDCustomer";
                    IDCustomer.DataBind();
                }

            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                ClassUser classUser = new ClassUser();
                Transaction transaction = new Transaction
                {
                     
                    Date =  DateTime.Now.Date,
                    IDUser = 9,//classUser.IDUserlogin,
                    IDCustomer = 9,//int.Parse(IDCustomer.SelectedValue),
                    Discount = 9000,//int.Parse(Discount.Text),
                    Subtotal = 90000,//int.Parse(Subtotal.Text),
                    Grandtotal =90000, //int.Parse(Grandtotal.Text),
                    CreateBy = "1",
                    CreateAt = DateTime.Now
                 };

                db.Transactions.InsertOnSubmit(transaction);
                db.SubmitChanges();

                Response.Redirect("create_transaksi.aspx");
            }
        }
    }
}