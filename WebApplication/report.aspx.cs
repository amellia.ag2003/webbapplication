﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication;

public partial class report : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                Tanggal.Text = DateTime.Now.ToString("dd/MM/yyyy");

                DataTransaksi.DataSource = db.Transactions
                    .Select(item => new
                    {
                        Customer = item.Customer.Name,
                        item.Date,
                        item.Discount,
                        item.Subtotal,
                        item.Grandtotal
                    }).ToArray();
                DataTransaksi.DataBind();
            }
        }
    }

    private void loaddata(DateTime date)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            DataTransaksi.DataSource = db.Transactions.Where(item => item.Date == date)
                    .Select(item => new
                    {
                        Customer = item.Customer.Name,
                        item.Date,
                        item.Discount,
                        item.Subtotal,
                        item.Grandtotal
                    }).ToArray();
            DataTransaksi.DataBind();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }

    protected void Submit_Click(object sender, EventArgs e)
    {

    }


    protected void Cari_Click(object sender, EventArgs e)
    {
        DateTime date = DateTime.Parse(Tanggal.Text);

        loaddata(date.Date);
    }
}