﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


    public partial class AddDataUaser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                User user = new User
                {

                    Name = Name.Text,
                    Username = Username.Text,
                    Password = Password.Text,
                    Flag = true,
                    CreatedBy = "1",
                    CreatedAt = DateTime.Now
                };

                db.Users.InsertOnSubmit(user);
                db.SubmitChanges();

                Response.Redirect("Users.aspx");
            }
        }
    }
