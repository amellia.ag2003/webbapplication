﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AddDataProductD : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                Product.DataSource = db.Products.ToArray();
                Product.DataTextField = "Name";
                Product.DataValueField = "IDProduct";
                Product.DataBind();
            }

        }

    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            ProductDetail productDetail = new ProductDetail
            {
                IDProduct = int.Parse(Product.SelectedValue),
                Variant = Variant.Text,
                Qty = int.Parse(Qty.Text),
                Price = int.Parse(Price.Text),
                Flag = true,
                CreatedBy = "1",
                CreatedAt = DateTime.Now
            };

            db.ProductDetails.InsertOnSubmit(productDetail);
            db.SubmitChanges();

            Response.Redirect("ProductD.aspx");
        }
    }
}