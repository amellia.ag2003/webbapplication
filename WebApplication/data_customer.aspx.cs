﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


    public partial class data_customer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            Customer customer = db.Customers.FirstOrDefault(item => item.IDCustomer == int.Parse(Request.QueryString["id"]));

            if (customer != null)
            {
                IDCustomer.Text = customer.IDCustomer.ToString();
                Name.Text = customer.Name;

                Submit.Text = "Ubah";
            }
            else
            {
                //
            }
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            if (IDCustomer.Text == string.Empty)
            {
                Customer customer = new Customer
                {
                    Name = Name.Text,
                    Flag = true,
                    CreatedBy = "1",
                    CreatedAt = DateTime.Now
                };

                db.Customers.InsertOnSubmit(customer);
            }
            else
            {
                Customer customer = db.Customers.FirstOrDefault(item => item.IDCustomer == int.Parse(IDCustomer.Text));

                customer.Name = Name.Text;

            }

            db.SubmitChanges();

            Response.Redirect("Customers.aspx");
        }
    }
}