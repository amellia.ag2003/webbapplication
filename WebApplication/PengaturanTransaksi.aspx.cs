﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PengaturanTransaksi : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                IDCustomer.DataSource = db.Customers.ToArray();
                IDCustomer.DataTextField = "Name";
                IDCustomer.DataValueField = "IDCustomer";
                IDCustomer.DataBind();

                IDUsers.DataSource = db.Users.ToArray();
                IDUsers.DataTextField = "Name";
                IDUsers.DataValueField = "IDUser";
                IDUsers.DataBind();
            }

        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            //ClassUser classUser = new ClassUser();
            Transaction transaction = new Transaction
            {

                Date = DateTime.Now.Date,
                IDUser = int.Parse(IDUsers.SelectedValue),
                IDCustomer = int.Parse(IDCustomer.SelectedValue),
                Discount = 9000,//int.Parse(Discount.Text),
                Subtotal = 90000,//int.Parse(Subtotal.Text),
                Grandtotal = 90000, //int.Parse(Grandtotal.Text),
                CreateBy = "1",
                CreateAt = DateTime.Now
            };

            db.Transactions.InsertOnSubmit(transaction);
            db.SubmitChanges();

            Response.Redirect("create_transaksi.aspx");
        }
    }
}