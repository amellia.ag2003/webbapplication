﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication
{
    public partial class register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Register_Click(object sender, EventArgs e)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                User user = new User
                {
                    Name = Name.Text,
                    Username = Username.Text,
                    Password = Password.Text ,
                    Flag = true ,
                    CreatedBy = "1" ,
                    CreatedAt = DateTime.Now
                };

                db.Users.InsertOnSubmit(user);
                db.SubmitChanges();

                Response.Redirect("login.aspx");
            }
        }
    }
}