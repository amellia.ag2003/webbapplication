﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication
{
    public partial class transaksi_details : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    var _result = db.TransactionDetails.Select(item => new
                    {
                        Product = item.Product,
                        Qty = item.Qty,
                        Price = item.Price,
                        Discount = item.Discount,
                        Subtotal = item.Subtotal
                    });

                    DataTransaksiDetails.DataSource = _result;
                    DataTransaksiDetails.DataBind();
                }
            }
        }
    }
}