﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


    public partial class AddDataProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                Product product = new Product
                {
                    SKU = SKU.Text,
                    Name = Name.Text,
                    Flag = true,
                    CreatedBy = "1",
                    CreatedAt = DateTime.Now
                };

                db.Products.InsertOnSubmit(product);
                db.SubmitChanges();

                Response.Redirect("product.aspx");
            }
        }
    }
