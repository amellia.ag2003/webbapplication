﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class product : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loadData();

        }
    }

    private void loadData()
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            DataProduct.DataSource = db.Products.Select(item => new { item.IDProduct, item.SKU, item.Name }).ToArray();
            DataProduct.DataBind();
        }


    }

    protected void DataProduct_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Ubah")
        {

        }
        else if (e.CommandName == "Delete")
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                Product product = db.Products.FirstOrDefault(item => item.IDProduct == int.Parse(e.CommandArgument.ToString()));

                if (product != null)
                {
                    var data = db.ProductDetails.Where(x => x.IDProduct == product.IDProduct).ToArray();

                    db.Products.DeleteOnSubmit(product);
                    db.ProductDetails.DeleteAllOnSubmit(data);
                }

                db.SubmitChanges();
                loadData();

            }
        }
    }
}
