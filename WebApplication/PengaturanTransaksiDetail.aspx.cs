﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PengaturanTransaksiDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                IDTransaction.DataSource = db.Customers.ToArray();
                IDTransaction.DataTextField = "Name";
                IDTransaction.DataValueField = "IDCustomer";
                IDTransaction.DataBind();

                IDProductDetails.DataSource = db.Products.ToArray();
                IDProductDetails.DataTextField = "Name";
                IDProductDetails.DataValueField = "IDProduct";
                IDProductDetails.DataBind();

                //var result = db.TransactionDetails.ToArray();


                //var Result = result.Select(item => new
                //{
                //    NamaProduk = item.ProductDetail.Product.Name,
                //    Qty = item.Qty,
                //    Price = item.Price,
                //    Discount = item.Discount,
                //    Subtotal = item.Price + item.Discount,
                //}).ToArray();


                //IDProductDetails.DataSource = Result;
                //IDProductDetails.DataBind();
            }

        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            TransactionDetail transactionD = new TransactionDetail
            {
                IDTransaction = int.Parse(IDTransaction.SelectedValue),
                IDProductDetails = int.Parse(IDProductDetails.SelectedValue),
                Qty = int.Parse(Subtotal.Text),
                Price = int.Parse(Price.Text),
                Discount = int.Parse(Discount.Text),
                Subtotal = int.Parse(Subtotal.Text)
            };

            db.TransactionDetails.InsertOnSubmit(transactionD);
            db.SubmitChanges();

            Response.Redirect("transaksi_details.aspx");
        }
    }
}