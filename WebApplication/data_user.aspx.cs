﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class data_user : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            User user = db.Users.FirstOrDefault(item => item.IDUser == int.Parse(Request.QueryString["id"]));

            if (user != null)
            {
                IDUser.Text = user.IDUser.ToString();
                Name.Text = user.Name;
                Username.Text = user.Username;

                Submit.Text = "Ubah";
            }
            else
            {
                //
            }
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            if (IDUser.Text == string.Empty)
            {
                User user = new User
                {
                    Name = Name.Text,
                    Username = Username.Text,
                    Password = Password.Text,
                    Flag = true,
                    CreatedBy = "1",
                    CreatedAt = DateTime.Now
                };

                db.Users.InsertOnSubmit(user);
            }
            else
            {
                User user = db.Users.FirstOrDefault(item => item.IDUser == int.Parse(IDUser.Text));

                user.Name = Name.Text;
                user.Username = Username.Text;
                user.Password = Password.Text;
            }

            db.SubmitChanges();

            Response.Redirect("Users.aspx");
        }
    }
}