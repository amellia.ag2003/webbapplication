﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


    public partial class Customers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadData();
            }
        }

        private void loadData()
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                DataCustomer.DataSource = db.Customers.Select(item => new { item.IDCustomer, item.Name}).ToArray();
                DataCustomer.DataBind();
            }
        }

        protected void DataCustomer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        if (e.CommandName == "Ubah")
        {

        }
        else if (e.CommandName == "Delete")
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                Customer customer = db.Customers.FirstOrDefault(item => item.IDCustomer == int.Parse(e.CommandArgument.ToString()));

                if (customer != null)
                {
                    var data = db.Transactions.Where(x => x.IDCustomer == customer.IDCustomer).ToArray();

                    db.Customers.DeleteOnSubmit(customer);
                    db.Transactions.DeleteAllOnSubmit(data);
                }

                   
                db.SubmitChanges();
                loadData();

            }
        }
    }

}
