﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


    public partial class create_transaksi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (DatabaseDataContext db = new DatabaseDataContext())
                {


                    DataTransaksi.DataSource = db.Transactions.ToArray();
                    DataTransaksi.DataBind();
                }
            }
        }
    }
