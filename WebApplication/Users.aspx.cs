﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Users : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loadData();
        }
    }

    private void loadData()
    {
        using (DatabaseDataContext db = new DatabaseDataContext())
        {
            DataUser.DataSource = db.Users.Select(item => new { item.IDUser, item.Name, item.Username, item.Password }).ToArray();
            DataUser.DataBind();
        }
    }

    protected void DataUser_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Ubah")
        {

        }
        else if (e.CommandName == "Delete")
        {
            using (DatabaseDataContext db = new DatabaseDataContext())
            {
                User user = db.Users.FirstOrDefault(item => item.IDUser == int.Parse(e.CommandArgument.ToString()));

                if (user != null)
                {
                    var data = db.Transactions.Where(x => x.IDUser == user.IDUser).ToArray();

                    db.Users.DeleteOnSubmit(user);
                    db.Transactions.DeleteAllOnSubmit(data);
                }

                db.SubmitChanges();

                loadData();
            }

        }
    }
}